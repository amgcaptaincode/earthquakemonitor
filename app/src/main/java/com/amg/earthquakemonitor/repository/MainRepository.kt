package com.amg.earthquakemonitor.repository

import androidx.lifecycle.LiveData
import com.amg.earthquakemonitor.Earthquake
import com.amg.earthquakemonitor.api.EarthquakeJsonResponse
import com.amg.earthquakemonitor.api.service
import com.amg.earthquakemonitor.database.EarthquakeDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MainRepository(private val database: EarthquakeDatabase) {

    suspend fun fetchEarthquake(sortByMagnitude : Boolean) : MutableList<Earthquake> {
        return withContext(Dispatchers.IO) {

            val earthquakeJsonResponse = service.getLastHourEarthquake()
            val earthquakeList = parseEarthquake(earthquakeJsonResponse)

            database.eqDao.insertAll(earthquakeList)

            fetchEarthquakeFromDB(sortByMagnitude)

        }

    }
    suspend fun fetchEarthquakeFromDB(sortByMagnitude : Boolean) : MutableList<Earthquake> {
        return withContext(Dispatchers.IO) {
            if (sortByMagnitude) {
                database.eqDao.getEarthquakesByMagnitude()
            } else{
                database.eqDao.getEarthquakes()
            }
        }
    }

    private fun parseEarthquake(earthquakeJsonResponse: EarthquakeJsonResponse): MutableList<Earthquake> {

        val earthquakeList = mutableListOf<Earthquake>()
        val featureList = earthquakeJsonResponse.features
        for (feature in featureList) {
            val id = feature.id
            val properties = feature.properties
            val geometry = feature.geometry

            val magnitude = properties.mag
            val place = properties.place
            val time = properties.time

            val latitude = geometry.latitude
            val longitude = geometry.longitude

            earthquakeList.add(
                Earthquake(
                    id,
                    place,
                    magnitude,
                    time,
                    latitude,
                    longitude
                )
            )
        }

        return earthquakeList

    }

}