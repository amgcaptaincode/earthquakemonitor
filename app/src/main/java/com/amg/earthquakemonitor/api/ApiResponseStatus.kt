package com.amg.earthquakemonitor.api

enum class ApiResponseStatus {
    DONE, LOADING, ERROR, NOT_INTERNET_CONNECTION
}