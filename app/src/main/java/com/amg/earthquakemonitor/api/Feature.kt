package com.amg.earthquakemonitor.api

class Feature(val id: String, val properties: Properties, val geometry: Geometry)