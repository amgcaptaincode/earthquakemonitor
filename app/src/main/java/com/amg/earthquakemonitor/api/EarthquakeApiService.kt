package com.amg.earthquakemonitor.api

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET


interface EarthquakeApiService {

    @GET("all_hour.geojson")
    suspend fun getLastHourEarthquake(): EarthquakeJsonResponse


}

private var retrofit = Retrofit.Builder()
    .baseUrl("https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/")
    .addConverterFactory(MoshiConverterFactory.create())
    .build()

var service: EarthquakeApiService = retrofit.create(
    EarthquakeApiService::class.java)