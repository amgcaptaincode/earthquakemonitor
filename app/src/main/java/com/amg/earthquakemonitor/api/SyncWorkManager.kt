package com.amg.earthquakemonitor.api

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.amg.earthquakemonitor.database.getDatabase
import com.amg.earthquakemonitor.repository.MainRepository

class SyncWorkManager(appContext: Context, params : WorkerParameters) : CoroutineWorker(appContext, params) {

    companion object {
       const val WORKER_NAME = "SyncWorkManager"
    }

    private val database = getDatabase(appContext)
    private val mainRepository = MainRepository(database)

    override suspend fun doWork(): Result {
        mainRepository.fetchEarthquake(true)

        return Result.success()
    }

}