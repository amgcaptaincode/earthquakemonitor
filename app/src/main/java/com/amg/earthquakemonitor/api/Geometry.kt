package com.amg.earthquakemonitor.api

class Geometry(private val coordinates: Array<Double>) {

    val latitude: Double
        get() = coordinates.get(1)

    val longitude: Double
        get() = coordinates.get(0)
}