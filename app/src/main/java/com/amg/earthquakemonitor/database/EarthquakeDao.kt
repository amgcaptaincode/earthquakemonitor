package com.amg.earthquakemonitor.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.amg.earthquakemonitor.Earthquake

@Dao
interface EarthquakeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(eqList: MutableList<Earthquake>)

    @Query("SELECT * FROM earthquakes")
    fun getEarthquakes() : MutableList<Earthquake>

    @Query("SELECT * FROM earthquakes ORDER BY magnitude ASC")
    fun getEarthquakesByMagnitude() : MutableList<Earthquake>
}