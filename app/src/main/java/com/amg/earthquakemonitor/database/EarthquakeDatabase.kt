package com.amg.earthquakemonitor.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.amg.earthquakemonitor.Earthquake

@Database(entities = [Earthquake::class], version = 1)
abstract class EarthquakeDatabase : RoomDatabase() {

    abstract val eqDao: EarthquakeDao

}


private lateinit var INSTANCE: EarthquakeDatabase

fun getDatabase(context: Context): EarthquakeDatabase {
    synchronized(EarthquakeDatabase::class.java) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room.databaseBuilder(
                context.applicationContext,
                EarthquakeDatabase::class.java,
                "earthquake_db"
            ).build()
        }

        return INSTANCE
    }
}