package com.amg.earthquakemonitor

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.amg.earthquakemonitor.databinding.ActivityEarthquakeDetailBinding
import java.text.SimpleDateFormat
import java.util.*

class EarthquakeDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityEarthquakeDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var extras = intent.extras

        var eq = extras?.getParcelable<Earthquake>("earthquake")

        binding.tvMagnitudeDetail.text = eq?.magnitude.toString()
        binding.tvLatitudeDetail.text = eq?.latitude.toString()
        binding.tvLongitudeDetail.text = eq?.longitude.toString()
        binding.tvPlaceDetail.text = eq?.place

        val simpleDateFormat = SimpleDateFormat("dd/MMM/yyyy hh:mm:ss", Locale.getDefault())
        val date = Date(eq!!.time)
        binding.tvDateDetail.text = simpleDateFormat.format(date)
    }
}