package com.amg.earthquakemonitor.main

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.amg.earthquakemonitor.Earthquake
import com.amg.earthquakemonitor.api.ApiResponseStatus
import com.amg.earthquakemonitor.database.getDatabase
import com.amg.earthquakemonitor.repository.MainRepository
import kotlinx.coroutines.*
import java.net.UnknownHostException

private val TAG = MainViewModel::class.java.simpleName

class MainViewModel(application: Application, private val  sortType: Boolean) : AndroidViewModel(application) {

    // Otra forma de crear coroutines
    /*private val job = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + job)*/

    private val database = getDatabase(application)
    private val mainRepository = MainRepository(database)

    private val _status = MutableLiveData<ApiResponseStatus>()
    val status: LiveData<ApiResponseStatus>
        get() = _status

    private var _eqList = MutableLiveData<MutableList<Earthquake>>()
    val eqList: LiveData<MutableList<Earthquake>>
        get() = _eqList

    init {

        // Otra forma de crear coroutines
        /*coroutineScope.launch {
            _eqList.value = fetchEarthquake()
        }*/

        reloadEarthquakesFromDB(sortType)
    }

    private fun reloadEarthquakes() {
        viewModelScope.launch {
            try {
                _status.value = ApiResponseStatus.LOADING
                _eqList.value = mainRepository.fetchEarthquake(sortType)
                _status.value = ApiResponseStatus.DONE
            } catch (e: UnknownHostException) {
                _status.value = ApiResponseStatus.NOT_INTERNET_CONNECTION
                Log.d(TAG, "No internet connection.", e);
            }
        }
    }

    fun reloadEarthquakesFromDB(sortByMagnitude: Boolean) {
        viewModelScope.launch {
            _eqList.value = mainRepository.fetchEarthquakeFromDB(sortByMagnitude)
            if (_eqList.value!!.isEmpty()) {
                reloadEarthquakes()
            }
        }
    }

    // Otra forma de crear coroutines
    /*override fun onCleared() {
        super.onCleared()
        job.cancel()
    }*/

}