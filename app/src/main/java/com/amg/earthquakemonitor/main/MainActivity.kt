package com.amg.earthquakemonitor.main

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.amg.earthquakemonitor.Earthquake
import com.amg.earthquakemonitor.EarthquakeDetailActivity
import com.amg.earthquakemonitor.R
import com.amg.earthquakemonitor.api.ApiResponseStatus
import com.amg.earthquakemonitor.api.WorkerUtil
import com.amg.earthquakemonitor.databinding.ActivityMainBinding

private const val SORT_TYPE = "sort_type"
class MainActivity : AppCompatActivity(), EarthquakeAdapter.EarthquakeSelectListener {

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.rvEarthquake.layoutManager = LinearLayoutManager(this)

        WorkerUtil.scheduleSync(this)
        val sortType = getSortType()

        viewModel = ViewModelProvider(this, MainViewModelFactory(application, sortType)).get(MainViewModel::class.java)

        val adapter = EarthquakeAdapter(this)
        binding.rvEarthquake.adapter = adapter

        viewModel.eqList.observe(this, Observer { earthquakeList ->
            adapter.submitList(earthquakeList)
            handleEmptyList(earthquakeList, binding)
        })

        viewModel.status.observe(this, Observer { apiResponseStatus ->
            if (apiResponseStatus == ApiResponseStatus.DONE || apiResponseStatus == ApiResponseStatus.NOT_INTERNET_CONNECTION) {
                binding.progressWheel.visibility = GONE
            } else if (apiResponseStatus == ApiResponseStatus.LOADING) {
                binding.progressWheel.visibility = VISIBLE
            }
        })

    }

    private fun handleEmptyList(earthquakeList: List<Earthquake>, binding: ActivityMainBinding) {
        if (earthquakeList.isEmpty()) {
            binding.rvEarthquake.visibility = GONE
            binding.tvNoEarthquake.visibility = VISIBLE
        } else {
            binding.rvEarthquake.visibility = VISIBLE
            binding.tvNoEarthquake.visibility = GONE
        }
    }

    override fun onEarthquakeSelected(earthquake: Earthquake) {
        var intent = Intent(this, EarthquakeDetailActivity::class.java)
        intent.putExtra("earthquake", earthquake)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.earthquake_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val itemId = item.itemId

        if (itemId == R.id.menu_sort_magnitude) {
            viewModel.reloadEarthquakesFromDB(true)
            sortType(true)
        } else if (itemId == R.id.menu_sort_time) {
            viewModel.reloadEarthquakesFromDB(false)
            sortType(false)
        }

        return super.onOptionsItemSelected(item)
    }

    private fun sortType(sortByMagnitude : Boolean) {
        val prefs = getPreferences(Context.MODE_PRIVATE)
        val edit = prefs.edit()
        edit.putBoolean(SORT_TYPE, sortByMagnitude)
        edit.apply()
    }

    private fun getSortType() : Boolean {
        val prefs = getPreferences(Context.MODE_PRIVATE)
        return prefs.getBoolean(SORT_TYPE, false)
    }
}