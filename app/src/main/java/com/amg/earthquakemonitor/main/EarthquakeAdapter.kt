package com.amg.earthquakemonitor.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.amg.earthquakemonitor.Earthquake
import com.amg.earthquakemonitor.R

class EarthquakeAdapter(val earthquakeSelectListener: EarthquakeSelectListener) :
    ListAdapter<Earthquake, EarthquakeAdapter.EarthquakeViewHolder>(
        DiffCallbacks
    ) {

    companion object DiffCallbacks : DiffUtil.ItemCallback<Earthquake>() {
        override fun areItemsTheSame(oldItem: Earthquake, newItem: Earthquake): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Earthquake, newItem: Earthquake): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EarthquakeViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_earthquake, parent, false)
        return EarthquakeViewHolder(view)
    }

    override fun onBindViewHolder(holder: EarthquakeViewHolder, position: Int) {
        val earthquake = getItem(position)
        holder.onBind(earthquake)
    }

    inner class EarthquakeViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val tvMagnitude = view.findViewById<TextView>(R.id.tv_magnitude)
        val tvPlace = view.findViewById<TextView>(R.id.tv_place)


        fun onBind(earthquake: Earthquake) {
            tvMagnitude.text = itemView.context.getString(R.string.magnitude_format, earthquake.magnitude)
            tvPlace.text = earthquake.place

            itemView.setOnClickListener {
                //Toast.makeText(itemView.context, earthquake.place, Toast.LENGTH_SHORT).show()
                earthquakeSelectListener.onEarthquakeSelected(earthquake)
            }

        }

    }

    interface EarthquakeSelectListener {
        fun onEarthquakeSelected(earthquake: Earthquake)
    }

}